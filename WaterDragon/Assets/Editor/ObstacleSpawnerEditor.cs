﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ObstacleSpawner))]
public class ObstacleSpawnerEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		ObstacleSpawner myScript = (ObstacleSpawner)target;
		if(GUILayout.Button("Generate Creature"))
		{
			myScript.GenerateObstacle();
		}
	}
}