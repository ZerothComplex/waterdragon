﻿using UnityEngine;
using System.Collections;

public static class EnumCollection {

	public enum Direction
	{
		Left = -1,
		Right = 1
	}

	public enum Size
	{
		S = 1,
		M = 2,
		L = 3,
		XL = 4
	}

}
