﻿using UnityEngine;
using System.Collections;

public class ObstacleSpawner : MonoBehaviour {

	[SerializeField]
	private Transform leftSpawn;
	[SerializeField]
	private Transform rightSpawn;

	[SerializeField]
	private GameObject creaturePrefab;

	[SerializeField]
	private EnumCollection.Direction direction;

	[SerializeField]
	private float speed;

	[SerializeField]
	private EnumCollection.Size size;

	void Start () 
	{
		Physics2D.IgnoreLayerCollision(8, 8, true);
	
	}
	
	public void GenerateObstacle()
	{
		Transform spawnPosition;
		if (direction == EnumCollection.Direction.Right)
			spawnPosition = leftSpawn;
		else
			spawnPosition = rightSpawn;


		var obstacleObj = (GameObject)Instantiate (creaturePrefab, spawnPosition.position , Quaternion.identity);

		var obstacle = obstacleObj.GetComponent<Obstacle> ();

		obstacle.Size = size;
		obstacle.Direction = direction;
		obstacle.Speed = speed;

	}
}
