﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	[SerializeField]
	private GameObject player;

	[SerializeField]
	private float force;

	private Rigidbody2D playerRigidbody;

	// Use this for initialization
	void Start ()
	{
		playerRigidbody = player.GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if (Input.GetMouseButton(0))
		{
			playerRigidbody.AddForce (Vector2.up * force * Time.deltaTime, ForceMode2D.Impulse);	
		}
	}


}
