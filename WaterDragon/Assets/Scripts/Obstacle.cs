﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {

	private float speed;
	public float Speed { get {return speed;} set{speed = value;} }

	private EnumCollection.Direction direction;
	public EnumCollection.Direction Direction { get {return direction;} set{direction = value;} }


	private EnumCollection.Size size;
	public EnumCollection.Size Size { get {return size;} set{transform.localScale = Vector2.one * (int)value;} }

	private Rigidbody2D rigidboy2D;

	Vector2 moveDirection;

	void Start () 
	{
		rigidboy2D = GetComponent<Rigidbody2D> ();
	}
	
	void FixedUpdate () 
	{
		moveDirection = Vector2.right * (int)direction;
		Debug.Log (moveDirection);
		rigidboy2D.AddForce (new Vector2((int)direction,0) * speed * Time.deltaTime, ForceMode2D.Impulse);
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag ("Boundary")) 
		{
			if (direction == EnumCollection.Direction.Left) 
				direction = EnumCollection.Direction.Right;
			else
				direction = EnumCollection.Direction.Left;
		}
	}
}
